# frozen_string_literal: true

RSpec.describe Gcs::SbomConverter do
  describe '#convert' do
    subject(:gitlab_format) { described_class.new(report).convert }

    let(:properties) { JSON.parse(gitlab_format)["metadata"]["properties"] }

    context 'when image has an OS' do
      let(:report) { fixture_file_json_content('trivy-sbom.json').to_json }

      it 'prepares report with schema version' do
        expect(property(described_class::PROPERTY_NAME_SCHEMA_VERSION)).to eq("1")
      end

      it 'prepares report image name' do
        expect(property(described_class::PROPERTY_NAME_IMAGE_NAME)).to eq("photon")
      end

      it 'prepares report image tag' do
        expect(property(described_class::PROPERTY_NAME_IMAGE_TAG)).to eq("5.0-20231007")
      end

      it 'prepares report operating system name' do
        expect(property(described_class::PROPERTY_NAME_OPERATING_SYSTEM_NAME)).to eq("Photon OS")
      end

      it 'prepares report operating system version' do
        expect(property(described_class::PROPERTY_NAME_OPERATING_SYSTEM_VERSION)).to eq("5.0")
      end

      it 'preserves existing metadata properties' do
        expect(property("existing property")).to eq("I love writing tests")
      end
    end

    context 'when the image does not have an OS' do
      let(:report) { fixture_file_json_content('trivy-scratch-sbom.json').to_json }

      it 'prepares report image name' do
        expect(property(described_class::PROPERTY_NAME_IMAGE_NAME)).to eq("registry.gitlab.com/secure/cs-scratch/main")
      end

      it 'prepares report image tag' do
        expect(property(described_class::PROPERTY_NAME_IMAGE_TAG)).to eq("60c1ced811b037afdd106383d6ea7c6c2a4609cd")
      end

      it 'does not include operating system property' do
        expect(property(described_class::PROPERTY_NAME_OPERATING_SYSTEM)).to be_nil
      end
    end

    context 'when trivy generates a CycloneDX specVersion 1.5 sbom' do
      let(:report) { fixture_file_json_content('trivy-sbom-1.5.json').to_json }

      subject(:converted) { JSON.parse(described_class.new(report).convert) }

      it 'gets converted into specVersion 1.4' do
        expect(converted).to match_cyclonedx_schema('1.4')
      end
    end

    def property(name)
      property = properties.find { |item| item["name"] == name }
      property&.dig('value')
    end
  end
end
