# frozen_string_literal: true

RSpec.describe 'trivy java db' do
  before(:all) do
    setup_schemas!
  end

  context 'when scanning an image with a custom CS_TRIVY_JAVA_DB value', integration: :trivy_java_db do
    subject(:report) { runner.report_for(type: 'container-scanning') }

    before(:all) do
      runner.scan(
        env: {
          'CS_TRIVY_JAVA_DB' => 'ghcr.test/aquasecurity/trivy-java-db',
          'CS_IMAGE' => 'docker.test/webgoat/webgoat-8.0:latest',
          'ADDITIONAL_CA_CERT_BUNDLE' => x509_certificate.read,
          'CS_DISABLE_DEPENDENCY_LIST' => 'false'
        }
      )
    end

    specify { expect(report).to match_security_report_schema(:container_scanning) }
  end
end
